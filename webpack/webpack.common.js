const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

var VENDOR_LIBS = [
  "axios",
  'react',
  'react-dom',
  'react-loadable',
  'react-router',
  'react-router-dom',]
module.exports = {
  entry: {
    bundle: path.resolve(__dirname, '..', './src/index.tsx'),
    vendor: VENDOR_LIBS
  },
  // entry: path.resolve(__dirname, '..', './src/index.tsx'),
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
    plugins: [
      new TsconfigPathsPlugin()
    ],
  },
  module: {
    rules: [
      {
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
          },
        ],
      },
      {
        test: /\.(css|sass|scss|less)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              sourceMap: true
            }
          },
          
          {
            loader: 'less-loader', // compiles Less to CSS
            options: {
              lessOptions: { // If you are using less-loader@5 please spread the lessOptions to options directly
                modifyVars: {
                  'primary-color': '#0F1E36',
                  'link-color': '#0F1E36',
                  'border-radius-base': '2px',
                },
                javascriptEnabled: true,
              },
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          },
        ]
      },
      
      {
        test: /\.(?:ico|gif|png|jpg|jpeg)$/i,
        type: 'asset/resource',
      },
      {
        test: /\.(woff(2)?|eot|ttf|otf|svg|)$/,
        type: 'asset/inline',
      },
      //   {
      //     loader: 'file-loader',
      //     test: /\.gz$|\.jpe?g$|\.gif$|\.png$|\.svg$|\.woff$|\.woff2|\.eot$|.ttf$|\.wav$|\.mp3$|\.icon$|\?[a-z0-9]+?$/,
      //     query: {
      //         name: '[name]-[md5:hash:8].[ext]'
      //     }
      // },
    ],
  },
  output: {
    path: path.resolve(__dirname, '..', './dist'),
    filename: "[chunkhash].[chunkhash].js",
    chunkFilename: '[chunkhash].bundle.js',
    // filename: 'bundle.js',
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, '..', './src/index.html'),
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css"
    }),
  ],
  optimization: {
    runtimeChunk: 'single',
  },
  stats: 'errors-only',
}
