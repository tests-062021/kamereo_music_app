export interface IProps {
    key?: string,
    name?: string,
    match?: string,
    history?: string,
    location?: string,
}