import { useEffect } from 'react';
import { hot } from 'react-hot-loader/root';
import { HashRouter } from 'react-router-dom';
import { routerApp } from './app-routers';
import "./styles/app.style.scss"
import UtilCommons from './utils/common.utils';
import AxiosInterceptorInit from './utils/helper/http-intercepter';
const App = () => {
  useEffect(() => {
    AxiosInterceptorInit();
  }, []);
  return (
    <>
      <HashRouter key="hash-router-main">
        {/*<BrowserRouter key="hash-router-main">*/}
        {UtilCommons.ShowRouter(routerApp)}
        {/*</BrowserRouter>*/}
      </HashRouter>
    </>
  )
}

export default hot(App);
