import { IProps } from "@models/page-props.interface";

const PageNotFound = (props: IProps) => {
  return (
    <>
      <h4>Not Found Page</h4>
    </>
  );
};

export default PageNotFound;
