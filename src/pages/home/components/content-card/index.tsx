import { useState } from "react";
import { Input, Image } from "antd";
import "./style.scss";
import AppImgages from "@assets/images";
interface IProps {
  key?: string;
  src: string;
  title?: string;
  description?: string;
  data?: any;
  onClick:
    | ((e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void)
    | undefined;
}

const ContentCard = (props: IProps) => {
  const CATEGORY_TAG = "content-card";
  const [,] = useState(0);
  return (
    <div
      className="content-card"
      key={props.key ? CATEGORY_TAG + props.key : null}>
      <Image
        preview={false}
        className="search-bar--icon-search cursor-pointer-g"
        onClick={props.onClick}
        src={props.src}></Image>
      <div className="content-card--title cursor-pointer-g">
        {props.title || ""}
      </div>
      <div className="content-card--description cursor-pointer-g">
        {props.description || ""}
      </div>
    </div>
  );
};

export default ContentCard;
