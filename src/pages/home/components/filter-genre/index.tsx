import WrapperContent from "@shared/components/wrapper-content";
import { useState } from "react";
import CmsConfig from "src/utils/cms.config";
import CategoryTag from "../category-tag";
interface IProps {}

const FilterGenre = (props: IProps): JSX.Element => {
  const [,] = useState(0);
  return (
    <div className="filter">
      <WrapperContent title={CmsConfig.homePage.filter.title}>
        <div className="filter--tags-wrapper">
          <CategoryTag
            name="Blues"
            onClick={(value) => {
              console.log(value);
            }}></CategoryTag>
          <CategoryTag name="Classical"></CategoryTag>
          <CategoryTag name="Country"></CategoryTag>
          <CategoryTag name="Blues"></CategoryTag>
          <CategoryTag name="Blues"></CategoryTag>
          <CategoryTag name="Blues"></CategoryTag>
          <CategoryTag name="Blues"></CategoryTag>
          <CategoryTag name="Blues"></CategoryTag>
          <CategoryTag name="Reggae / Dancehall"></CategoryTag>
          <CategoryTag name="Blues"></CategoryTag>
          <CategoryTag name="Blues"></CategoryTag>
        </div>
      </WrapperContent>
    </div>
  );
};

export default FilterGenre;
