import AppImgages from "@assets/images";
import WrapperContent from "@shared/components/wrapper-content";
import { useState } from "react";
import CmsConfig from "src/utils/cms.config";
import ContentCard from "../content-card";
interface IProps {}

const ContentShow = (props: IProps): JSX.Element => {
  const [,] = useState(0);
  const handleClick = (e: any) => {};
  return (
    <div className="content-show">
      <WrapperContent title={`${CmsConfig.homePage.content.title}(${11})`}>
        <div className="content-show--intems-wrapper">
          <ContentCard
            title="I don't Care"
            description="Jonas Brothers"
            src={AppImgages.imageThumnailDemo2}
            onClick={handleClick}></ContentCard>
            <ContentCard
            title="I don't Care"
            description="Jonas Brothers"
            src={AppImgages.imageThumnailDemo3}
            onClick={handleClick}></ContentCard>
            <ContentCard
            title="I don't Care"
            description="Jonas Brothers"
            src={AppImgages.imageThumnailDemo4}
            onClick={handleClick}></ContentCard>
            <ContentCard
            title="I don't Care"
            description="Jonas Brothers"
            src={AppImgages.imageThumnailDemo5}
            onClick={handleClick}></ContentCard>
            <ContentCard
            title="I don't Care"
            description="Jonas Brothers"
            src={AppImgages.imageThumnailDemo1}
            onClick={handleClick}></ContentCard>
        </div>
      </WrapperContent>
    </div>
  );
};

export default ContentShow;
