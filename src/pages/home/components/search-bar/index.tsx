import { useMemo, useRef, useState } from "react";
import AppImgages from "@assets/images";
import { Input, Image } from "antd";
import "./styles.scss";
import CmsConfig from "src/utils/cms.config";

interface IProps {
  onSearch?: (value: string) => void;
}

const SearchBar = (props: IProps) => {
  // const [value, setValue] = useState(0)
  // const throttled = useRef(throttle(async (newValue) => console.log(newValue)))

  // useEffect(() => throttled.current(value), [value])

  const onChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    console.log(e.target.value);
  };

  return (
    <>
      <div className="search-bar">
        <Input
          size="large"
          className="search-bar--input"
          placeholder={CmsConfig.homePage.searchBar.placeHolder}
          prefix={
            <Image
              width={18}
              preview={false}
              className="search-bar--icon-search cursor-pointer-g"
              onClick={(_e: any) => {
                console.log(1);
              }}
              src={AppImgages.iconSearch}
            />
          }
          onChange={onChange}
        />
      </div>
    </>
  );
};

export default SearchBar;
