import { useState } from "react";
import "./style.scss";

interface IProps {
  key?: string;
  name: string;
  onClick?: (isSelected: boolean) => void;
}

const CategoryTag = (props: IProps) => {
  const CATEGORY_TAG = "categroy-tag";
  const [isSelect, setIsSelect] = useState(false);
  const hanldClick = () => {
    const _isSelect = isSelect;
    setIsSelect((preState) => !preState);
    props.onClick && props.onClick(!_isSelect);
  };
  return (
    <div
      key={props.key ? CATEGORY_TAG + props.key : null}
      onClick={hanldClick}
      className={`tags-wrapper cursor-pointer-g ${isSelect ? "selected" : ""}`}>
      {props.name || ""}
    </div>
  );
};

export default CategoryTag;
