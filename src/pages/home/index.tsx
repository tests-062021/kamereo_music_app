import { Divider } from "antd";
import { IProps } from "@models/page-props.interface";
import SearchBar from "./components/search-bar";
import FilterGenre from "./components/filter-genre";
import ContentShow from "./components/content-show";

const HomePage = (props: IProps) => (
  <>
    <SearchBar />
    <Divider />
    <FilterGenre></FilterGenre>
    <ContentShow></ContentShow>
  </>
);

export default HomePage;
