// import 'babel-polyfill';

import { Observable } from "rxjs";
import HttpHelpers from "src/utils/helper/http";

export const api = (): Observable<any> => {
    return HttpHelpers.get('/api/logincms');
}
