import { Route, Switch } from "react-router-dom";

const UtilCommons = {
    ShowRouter: (routers: Array<object>): JSX.Element => {
        const result = routers.map((routers: any, index: number) => {
            return (
                <Route
                    key={index}
                    path={routers.path}
                    exact={routers.exact}
                    component={routers.main}
                />
            )
        })
        return <Switch>{result}</Switch>
    }
}

export default UtilCommons;