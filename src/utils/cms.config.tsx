const CmsConfig = {
  homePage: {
    searchBar: {
      placeHolder: 'Search your entertainment ',
    },
    filter: {
      title: 'Filter Genre',
    },
    content: {
      title: 'Result ',
    },
  },
}
export default CmsConfig
