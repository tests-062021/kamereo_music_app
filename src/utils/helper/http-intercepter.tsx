import Axios from "axios-observable";



export default function AxiosInterceptorInit() {

    const token = localStorage.getItem('jwtToken') || '';

    Axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    Axios.defaults.headers.common['Accept-Language'] = 'USA';
    Axios.defaults.headers.common['Content-Type'] = "application/x-www-form-urlencoded,multipart/form-json1,text/plain";


    // Add a request interceptor
    Axios.interceptors.request.use(function (config: any) {
        // Do something before request is sent
        return config;
    }, function (error: any) {
        // Do something with request error
        return Promise.reject(error);
    });

    Axios.interceptors.response.use(function (response: { data: any; }) {
        // Do something with response json1
        return response.data || response;
    }, (error: { config: any; response: { status: any; data: { message: any; }; }; }) => {
        // xử lý lỗi 401
        if (error.config && error.response) {
            var status = error.response.status;
            switch (status) {
                case 401: {

                    break;
                }
                case 400: {

                    break;
                }

                case 403: {
                    break;
                }
                case 502: {
                    break;
                }
                case 500: {
                    break;
                }
                default: {
                }
            }
        } else {
            // toastWarning('Server 500 interval')
        }

        return Promise.reject(error);
    });

}