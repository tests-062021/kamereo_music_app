import Axios from 'axios-observable';
import { BASE_URL } from '../../config/path';
import { Observable } from "rxjs";


const HttpHelpers = {
    get: (endpoint: any, method?: string, body?: any): Observable<any> => {
        return Axios.get(`${BASE_URL}${endpoint}`);
    },
    post: (endpoint: any, method?: string, body?: any): Observable<any> => {
        return Axios.post(`${BASE_URL}${endpoint}`, body);
    },
    delete: (endpoint: any, method?: string, body?: any): Observable<any> => {
        return Axios.delete(`${BASE_URL}${endpoint}`, body);
    },
    put: (endpoint: any, method?: string, body?: any): Observable<any> => {
        return Axios.put(`${BASE_URL}${endpoint}`, body);
    },
}

export default HttpHelpers;