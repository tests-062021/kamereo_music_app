import Axios from 'axios-observable';

let flagRun404 = true;

export function setAuthorizationTokenObservable() {
    const token = localStorage.getItem('jwtToken') || '';
    const language = localStorage.getItem('language') || 'USA';
    // Axios.defaults.headers.common['Access-Control-Allow-Origin '] = "*";
    Axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    Axios.defaults.headers.common['Accept-Language'] = 'USA';
    // Axios.defaults.headers.common['Content-Type'] = "application/x-www-form-urlencoded,multipart/form-json1,text/plain";
    Axios.interceptors.response.use(function (response: { data: any; }) {
        // Do something with response json1
        return response.data || response;
    }, (error: { config: any; response: { status: any; data: { message: any; }; }; }) => {
        // xử lý lỗi 401
        if (error.config && error.response) {
            var status = error.response.status;
            switch (status) {
                case 401: {
                    // swalError('Phiên đăng nhập của bạn đã hết hạn!');
                    // window.location.href = `/#/login`;
                    window.location.href = `/cms/#/login`;

                    if (flagRun404) {
                        localStorage.clear();
                        const router = window.location.hash.split('#',)[1];
                        const mainRouter = router.split('/')[1] + '';
                        if ((['plan', 'task', 'noti'].indexOf(mainRouter) != -1)) {
                            window.location.href = `/#/login?${router}`;
                            // window.location.href = `/login?${router}`;
                        } else {
                            // window.location.href = `/#/login`;
                            window.location.href = `/cms/#/login`;
                        }
                        flagRun404 = false
                    }
                    break;
                }
                case 400: {
                    // notification['error']({
                    //     message: 'Error!',
                    //     description:
                    //     error.response.data.message
                    // });
                    break;
                }
                case 502: {
                    // notification['error']({
                    //     message: 'Error!',
                    //     description:
                    //     error.response.data.message
                    // });
                    break;
                }
                case 403: {
                    break;
                }
                default: {
                }
            }
        } else {
            // toastWarning('Server 500 interval')
        }
        if (token) {
            if (status != 403 && status != 401) {
                // swalError(error.response.data.message);
            }
        }

        return Promise.reject(error);
    });

}

export function setLanguageAxios(_language: string) {
    localStorage.setItem('language', _language);
    Axios.defaults.headers.common['Accept-Language'] = _language;
}

export function setTokenAxios(jwtToken: string) {
    flagRun404 = true
    localStorage.setItem('jwtToken', jwtToken);
    Axios.defaults.headers.common['Authorization'] = `Bearer ${jwtToken}`;
}

export function expiredToken() {
    localStorage.setItem('jwtToken', '');
    Axios.defaults.headers.common['Authorization'] = `Bearer ${''}`;
}
