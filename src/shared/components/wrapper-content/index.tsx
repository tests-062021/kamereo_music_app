import "./style.scss";

interface IProps {
  title: string;
  children?: any;
}

const WrapperContent = (props: IProps) => {
  return (
    <div className="wrapper-content">
      <div className="wrapper-content--title">{props.title || ""}</div>
      <div className="wrapper-content--child">{props.children}</div>
    </div>
  );
};

export default WrapperContent;
