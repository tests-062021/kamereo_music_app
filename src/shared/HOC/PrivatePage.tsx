import React from 'react';
import { Redirect } from "react-router";


const PrivatePage = (props: any) => {
    const token = localStorage.getItem('jwtToken');
    // const token:any ='123';
    return (
        <>
            {
                (token != null && token != '') ?
                    props.children
                    :
                    <Redirect
                        to={{
                            pathname: `/login`,
                        }}
                    />
            }
        </>
    )
}
export default PrivatePage;
