import { Redirect } from "react-router";


const PrivateLogin = (props: any) => {
    const token = localStorage.getItem('jwtToken');
    return (
        <>
            {
                (token == null || token == '') ?
                    props.children
                    :
                    <Redirect
                        to={{
                            pathname: '/declaration-list',
                        }}
                    />
            }
        </>
    )
}
export default PrivateLogin;
