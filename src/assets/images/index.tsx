import iconSearch from '@assets/images/icon/Search.png'
import imageThumnailDemo1 from '@assets/images/demo1.png'
import imageThumnailDemo2 from '@assets/images/demo2.png'
import imageThumnailDemo3 from '@assets/images/demo3.png'
import imageThumnailDemo4 from '@assets/images/demo4.png'
import imageThumnailDemo5 from '@assets/images/demo5.png'
const AppImgages = {
  iconSearch,
  imageThumnailDemo1,
  imageThumnailDemo2,
  imageThumnailDemo3,
  imageThumnailDemo4,
  imageThumnailDemo5,
}
export default AppImgages
