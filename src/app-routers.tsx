

import LoadingComponent from "./shared/components/global-loading";
import React from "react";
import Loadable from "react-loadable";
import { Redirect } from "react-router";

const HomePage = Loadable(
    {
        loader: () => import("./pages/home"),
        loading: LoadingComponent,
        delay: 2000
    });

const PageNotFound = Loadable(
    {
        loader: () => import("./pages/not-found"),
        loading: LoadingComponent,
        delay: 300
    });

//============>>>>>>>End Report Loadable<<<<<<<==================
export const routerApp: Array<object> = [
    {
        path: '/',
        exact: true,
        code: 'dashboard',
        main: ({ match, location, history }: any) =>
            <HomePage
                name={'Hook React component'}
                match={match}
                history={history}
                location={location} />
    },

    {
        path: '',
        exact: true,
        code: 'notfound',
        main: ({ match, location, history }: any) => <PageNotFound name={'Hook React component'}
            match={match}
            history={history}
            location={location} />
    }

]
